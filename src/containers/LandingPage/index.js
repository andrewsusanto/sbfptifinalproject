import React, { useState, useEffect } from "react";
import styled from "styled-components";

export const LandingPage = () => {
  const TodoContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    padding: 20px;
  `;

  const Todo = styled.div`
    padding: 20px;
  `;

  const [state, setState] = useState({
    loading: false,
    data: [],
  }); // list of todos

  const [nameToDoList, setToDoList] = useState("");
  const [tanggalToDoList, setTanggalToDoList] = useState("");
  const [jamToDoList, setJamToDoList] = useState("");
  const [liveClock, setClock] = useState(new Date());

  // const url = "https://pokeapi.co/api/v2/pokemon/ditto";
  const BACKEND_URL = "http://localhost:8000/todos/";
  // CRUD
  // create - POST /parent/
  // Read - GET /parent/
  // Update - PUT / PATCH /parent/id/
  // Delete - DELETE /parent/id/

  const onClick = () => {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        nama: nameToDoList,
        tanggal: tanggalToDoList,
        jam: jamToDoList,
      }),
    };
    fetch(BACKEND_URL, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        getData();
      })
      .catch();
  };

  useEffect(() => {
    getData();

    setInterval(function () {
      setClock(new Date());
    }, 1000);
  }, []);

  // read
  const getData = () => {
    setState({ loading: true });
    fetch(BACKEND_URL)
      .then((response) => response.json())
      .then((data) => {
        setState({ loading: false, data });
      })
      .catch();
  };

  // delete
  const deleteData = async (id) => {
    const requestOptions = {
      method: "DELETE",
    };

    await fetch(`${BACKEND_URL}${id}/`, requestOptions).catch();
    getData();
  };

  // update
  const finishData = async (id) => {
    const requestOptions = {
      method: "PATCH",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        is_done: true,
      }),
    };

    await fetch(`${BACKEND_URL}${id}/`, requestOptions).catch();
    getData();
  };

  return (
    <div>
      <h1>My To Do List | Clock : {liveClock.toString()}</h1>
      <TodoContainer>
        {state &&
          !state.loading &&
          state.data.map(({ id, nama, tanggal, jam, is_done }) => {
            console.log(state.data);
            return (
              <Todo>
                <h1>
                  {is_done ? <strike>{nama} </strike> : nama}
                  <button onClick={() => finishData(id)}>V</button>
                  <button onClick={() => deleteData(id)}>X</button>
                </h1>
                <p>{tanggal}</p>
                <p>{jam}</p>
              </Todo>
            );
          })}
      </TodoContainer>

      <h1>Add To Do List</h1>
      <table>
        <tr>
          <td>Nama To Do List</td>
          <td>
            <input
              type="text"
              placeholder="Nama To Do List"
              onChange={(e) => setToDoList(e.target.value)}
            />
          </td>
        </tr>
        <tr>
          <td>Tanggal</td>
          <td>
            <input
              type="date"
              placeholder="Tanggal"
              onChange={(e) => setTanggalToDoList(e.target.value)}
            />
          </td>
        </tr>
        <tr>
          <td>Jam</td>
          <td>
            <input
              type="time"
              placeholder="Waktu"
              onChange={(e) => setJamToDoList(e.target.value)}
            />
          </td>
        </tr>
        <button onClick={onClick}>Add To Do List</button>
      </table>
    </div>
  );
};
